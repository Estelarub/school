from odoo import fields, models

class Profesor(models.Model):
    _name = 'school.profesor'
    _description = 'Profesor'
    name = fields.Char('Nombre', required=True)
    primer_apellido = fields.Char(required=True)
    segundo_apellido = fields.Char(required=True)
    fecha_nac = fields.Date('Fecha de nacimiento')
    telefono = fields.Char();
    correo = fields.Char('Correo electronico')
    direccion = fields.Char('Dirección')
    poblacion = fields.Char('Población')
    baja = fields.Boolean('Baja?', default=True)

class Curso(models.Model):
    _name = 'school.curso'
    _description = 'Curso'
    name = fields.Char('Nombre', required=True)
    num_horas = fields.Integer('Número de horas')
    idioma = fields.Selection(
        [('val','Valenciano'),
         ('cas','Castellano'),
         ('ing','Ingles')])

class Horario(models.Model):
    _name = 'school.horario'
    _description = 'Horario'
    name = fields.Char('Nombre', required=True)
    hora_comienzo = fields.Datetime();
    hora_fin = fields.Datetime();