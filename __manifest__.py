# -*- coding: utf-8 -*-
{
    'name': "School Aplication",
    'description': 'Guarda la informacion de un centro de estudios',
    'author': 'Estela Rubio',
    'depends': ['base'],
    'application': True,
    'data': [
        'security/school_security.xml',
        'security/ir.model.access.csv',
        'views/school_menus.xml',
        'views/school_view.xml',
    ]
}
